﻿using System.Net;
using System.Windows.Forms;
using UDPServer.lib;

namespace UDPServer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            var server = new Server(IPAddress.Parse("127.0.0.1"), 13666);
            server.Start();
        }
    }
}
