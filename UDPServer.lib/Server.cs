﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CommonLibrary;

namespace UDPServer.lib
{
    public class Server
    {
        public IPAddress IPAddress { get; private set; }
        public int Port { get; private set; }

        private Thread ListenerThread { get; set; }
        private Timer SyncTimer { get; set; }
        private BlockingCollection<object> MessagePassing { get; set; } 

        private Dictionary<int,Player> PlayersDictionary { get; set; }

        public Server(IPAddress ipAddress, int port)
        {
            IPAddress = ipAddress;
            Port = port;
            MessagePassing = new BlockingCollection<object>();
            PlayersDictionary = new Dictionary<int, Player>();
            ListenerThread = new Thread(StartListeniner);
            ListenerThread.Start();
            SyncTimer = new Timer(SyncCallback);
            if ((bool) MessagePassing.Take())
            {
                SyncTimer.Change(0, 100);
            }
        }

        private void SyncCallback(object state)
        {
            Console.WriteLine("SyncCallback");
        }

        private void StartListeniner(object o)
        {
            MessagePassing.Add(true);

        }

        public void Start()
        {
            
        }
    }
}
